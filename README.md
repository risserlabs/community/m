# m

> lightweight wrapper around make

This program makes it easier to pass arguments to make targets
by sacrificing the ability to run multiple targets

## Setup

### Easy Install with Dependencies

```
$(curl --version >/dev/null 2>/dev/null && echo curl -L || echo wget -O-) https://gitlab.com/api/v4/projects/36388476/packages/generic/m/0.0.1/install.sh | sh
```

### Install

```
sudo make install
```

### Uninstall

```sh
sudo make uninstall
```

## Usage

```sh
m build --hello=world
```

this will end up running the following make command

```sh
make -s build ARGS="--hello=world"
```
