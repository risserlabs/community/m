include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/mkchain

export USER ?= nobody
export EMAIL ?= clayrisser@gmail.com
PKG_NAME ?= make-m
PKG_VERSION ?= 0.0.1
PKG_STRICT ?= 0
include $(MKPM)/pkg

DEPS_SKIP ?= 1
DEPS_AUTOINSTALL ?= 1
DEPS_APT += \
	devscripts \
	dh-make \
	dpkg-dev

ACTIONS += build
build/m:
	@$(call clear,build)
$(ACTION)/build: build/m $(call git_deps,.)
	@$(MKDIR) -p build
	@$(CP) m.sh build/m
	@$(CP) _m.sh build/_m
	@$(CHMOD) +x build/m
	@$(call done,build)

.PHONY: install +install
install: | ~build +install
+install: sudo
	@$(SUDO) $(MKDIR) -p /usr/local/share/zsh/site-functions
	@$(SUDO) $(CP) build/m /usr/local/bin/m
	@$(SUDO) $(CP) build/_m /usr/local/share/zsh/site-functions

.PHONY: uninstall
uninstall: sudo
	@$(SUDO) $(RM) -f /usr/local/bin/m
	@$(SUDO) $(RM) -f /usr/local/share/zsh/site-functions/_m

.PHONY: purge
purge: pkg-deb/clean
	@$(GIT) clean -fXd

.PHONY: clean
clean: ## clean project
	-@$(MKCACHE_CLEAN)
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

include $(MKPM)/deps

-include $(call actions)

endif
